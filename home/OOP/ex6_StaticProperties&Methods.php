<?php

class Student{

    public $name;
    public $dept;
    public static $age="23";
    public static $id="057";

    public function stddetails($a,$b){
        $this->name=$a;
        $this->dept=$b;

        echo "student name is {$this->name} <br/> department name is {$this->dept} <br/> Age is " . self::$age ;//static properties

    }
    public function stdId(){
        echo " Id is ". self::$id;
    }

}
$std=new Student();
$std->stddetails("amit","CSE");

Student::stdId();//static method
?>