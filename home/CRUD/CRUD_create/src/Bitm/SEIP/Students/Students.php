<?php
namespace App\Bitm\SEIP\Students;
use PDO;
class Students
{


    public $name;

    public function setData($data = '')
    {

        $this->name = $data['title'];
    }

    public function store()
    {

        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');
            $query = "INSERT INTO `students` (`id`, `title`) VALUES (:a,:b)";


            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':a' => null,
                ':b' => $this->name
            ));

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
}

//upto 27:38