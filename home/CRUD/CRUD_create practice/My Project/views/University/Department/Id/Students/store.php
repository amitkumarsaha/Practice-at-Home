<?php
//include_once ("../../../../../src/University/Department/Id/Students/Students.php");
include_once ("../../../../../vendor/autoload.php");
use Person\University\Department\Id\Students\Students;
$obj=new Students;
$obj->setData($_POST);

session_start();
if(!empty($_POST)) {
    if (preg_match("/([a-zA-Z])/", $_POST['title'])) {
        $_POST['title']= filter_var($_POST['title'],FILTER_SANITIZE_STRING);
        $obj->store();
        session_unset();
    }
    else{
        $_SESSION['msg']="Invalid input";
        header('location:Create.php');
    }
}
else{
    $_SESSION['msg']="flied can't be empty";
    header('location:Create.php');
}
?>