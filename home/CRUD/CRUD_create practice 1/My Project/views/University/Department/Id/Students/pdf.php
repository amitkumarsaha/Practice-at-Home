<?php
include_once ("../../../../../vendor/mpdf/mpdf/mpdf.php");
include_once ("../../../../../vendor/autoload.php");

use Person\University\Department\Id\Students\Students;
$obj=new Students;
$alldata=$obj->index();
$trs='';
$serial=0;
foreach ( $alldata as $data):
    $serial++;
    $trs.="<tr>";
    $trs.="<td>" .$serial. "</td>";
    $trs.="<td>" .$data['id']. "</td>";
    $trs.="<td>" .$data['title']. "</td>";
    $trs.="</tr>";

endforeach;
$html=<<<EOD
<!DOCTYPE html>
<html>
<head>
    <title>pdf.com</title>
</head>
<body>
<h1> List of Students</h1>

<table border="1"; >
    <thead>
    <tr>
        <th>Sl.</th>
        <th>ID</th>
        <th>Title</th>
    </tr>
    </thead>
    <tbody>
        $trs;
    </tbody>
</table>
</body>
EOD;

$mpdf=new mPDF();
$mpdf->WriteHTML($html);
$mpdf->output();

exit;

?>
